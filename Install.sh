#!/bin/bash
###
### The instructions followed in this script are taken directly from TP-Link
### HOWEVER, the instructions where last updated in 2019.....
### THIS MAY NOT WORK ON YOUR SYSTEM, I DON'T HAVE A LIST OF WORKING DISTROS YET.

OS=(cat /etc/os-release | grep "PRETTY_NAME")
Kernel_S=(uname -r | cut -d '.' -f 1-2)
KERNEL_F=(uname -r | cut -d '.' -f 1-4)
DRIVER=RTL8192EU
DIR=(pwd)

echo -e "Installation of TL-WN800ND Driver for debian based distros"
sleep 3 
echo -e "Compiling driver, please wait"


UBUNTU_OS() {
# Compiling the driver
make clean
make
## Loading the driver 
sudo cp 88x2bu.ko /lib/modules/$KERNEL_F/kernel/drivers/net/wireless
sudo depmod -a ### I believe this removes ALL other drivers installed.
sudo modprobe 88x2bu
sudo insmod 88x2bu.ko
}

RASPBERRY_OS() {
## Need to download kernel and other tools
cd
git clone https://github.com/raspberry/linux
git clone https://github.com/raspberry/tools
## Manufacturer refers to not recommended to use unzip
sudo jar -xf *.zip

##Now we need to modify the kernel 
cd linux
KERNEL=kernel7
make bcm2709_defconfig

## Kernel Compilation
make -j4 zImage modules dtbs
sudo make modules_install
sudo cp arch/arm/boot/dts/*dtb* /boot/
sudo cp arch/arm/boot/dts/overlays/*.dtb* /boot/overlays/
sudo cp arch/arm/boot/dts/overlays/README /boot/overlays/
sudo cp /arch/arm/boot/zImage /boot/$KERNEL.img
## NOTE: -j4 referes to using pi3 and 4 to compile to accelerate the compilation progress
## A REBOOT IS NECESSARY IN ORDER TO CONFIRM THAT KERNEL COMPILATION WAS SUCCESSFUL 
## ***TEST*** DRIVER COMPILATION IS STILL NEEDED, GOING TO TEST COMPILING IT BEFORE THE REBOOT
cd /home/pi/Desktop/rtl8192eu/Rasp
make clean
make 
sudo make install
}

if [[ $OS = "Raspberry*" ]]; then
	RASPBERRY_OS
else
	UBUNTU_OS
fi

echo "Your system will reboot now, cross your finger that it worked lol"
sleep 5

sudo reboot now

exit 0
